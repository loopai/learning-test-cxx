#include "Film.h"

Film::Film(std::string title, std::string year, std::string director, bool downloaded,
    std::set<std::string> genres) {
  m_title = title;
  m_year = year;
  m_director = director;
  m_downloaded = downloaded;
  m_genres = genres;
}

std::ostream& operator<< (std::ostream &out, Film &film) {
  out << "< Title: " << film.getTitle() << ", Year: " << film.getYear()
    << ", Director: " << film.getDirector() << ",\nDownloaded: ";
  if (film.isDownloaded()) {
    out << "yes,";
  } else {
    out << "no,";
  }
  out << " Genres: ";
  for (auto &it: film.getGenres()) { out << it << " "; }
  out << ">";
  return out;
}

