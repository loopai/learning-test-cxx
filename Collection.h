#ifndef COLLECTION_H
#define COLLECTION_H

#include <string>
#include <set>
#include "Film.h"

class Collection {
private:
  std::string m_name;
  std::set<Film *, cmp>* m_films;
public:
  Collection(std::string name): m_name(name) { m_films = new std::set<Film *, cmp>; }
  ~Collection() {
    while(!m_films->empty()) {
      auto it = m_films->begin();
      m_films->erase(it);
      delete *it;
    }
    if (m_films != nullptr) {
      delete m_films;
    }
  }
  std::string getName() { return m_name; }
  std::string getName() const {return m_name; }
  std::set<Film *, cmp> *getFilms() { return m_films; } 
  void addFilm(Film *film) { m_films->insert(film); }
  bool isInByTitle(std::string title);
  Film *queryByTitle(std::string title);
  void removeByTitle(std::string title);
  std::set<Film *, cmp> *queryByGenre(std::string genre);
};

#endif
